﻿using System.Collections.Generic;
using UnityEngine;

public class PossibleMovesCalculator
{
    public static List<Vector2> GetPossibleMoves(PieceType pieceType, Vector2 position, ColorEnum colorEnum)
    {
        switch (pieceType)
        {
            case PieceType.BISHOP:
                return GetBishopMoves(position, colorEnum);
            case PieceType.PAWN:
                return GetPawnMoves(position, colorEnum);
            case PieceType.KNIGHT:
                return GetKnightMoves(position, colorEnum);
            case PieceType.QUEEN:
                return GetQueenMoves(position, colorEnum);
            case PieceType.KING:
                return GetKingMoves(position, colorEnum);
            case PieceType.ROCK:
                return GetRockMoves(position, colorEnum);
            default:
                throw new MissingReferenceException("Bele fiqur yoxdu brat");
        }
    }

    private static List<Vector2> GetBishopMoves(Vector2 currentPosition, ColorEnum colorEnum)
    {
        List<Vector2> targets = new List<Vector2>();
        int i = (int)currentPosition.x;
        int j = (int)currentPosition.y;
        while (i < 7 && j < 7)
        {
            if (CheckNormalMove(i + 1, j + 1))
                targets.Add(new Vector2(++i, ++j));
            else
            {
                if (CheckAttackMove(i + 1, j + 1, colorEnum))
                    targets.Add(new Vector2(++i, ++j));
                break;
            }
        }
        i = (int)currentPosition.x;
        j = (int)currentPosition.y;
        while (i > 0 && j > 0)
        {
            if (CheckNormalMove(i - 1, j - 1))
                targets.Add(new Vector2(--i, --j));
            else
            {
                if (CheckAttackMove(i - 1, j - 1, colorEnum))
                    targets.Add(new Vector2(--i, --j));
                break;
            }
        }
        i = (int)currentPosition.x;
        j = (int)currentPosition.y;
        while (i > 0 && j < 7)
        {
            if (CheckNormalMove(i - 1, j + 1))
                targets.Add(new Vector2(--i, ++j));
            else
            {
                if (CheckAttackMove(i - 1, j + 1, colorEnum))
                    targets.Add(new Vector2(--i, ++j));
                break;
            }
        }
        i = (int)currentPosition.x;
        j = (int)currentPosition.y;
        while (i < 7 && j > 0)
        {
            if (CheckNormalMove(i + 1, j - 1))
                targets.Add(new Vector2(++i, --j));
            else
            {
                if (CheckAttackMove(i + 1, j - 1, colorEnum))
                    targets.Add(new Vector2(++i, --j));
                break;
            }
        }
        return targets;
    }
    private static List<Vector2> GetKingMoves(Vector2 currentPosition, ColorEnum colorEnum)
    {
        List<Vector2> targets = new List<Vector2>();
        int i = (int)currentPosition.x;
        int j = (int)currentPosition.y;
        if (i < 7 && j < 7 && (CheckNormalMove(i + 1, j + 1) || CheckAttackMove(i + 1, j + 1, colorEnum)))
        {
            targets.Add(new Vector2(i + 1, j + 1));
        }
        if (i < 7 && j > 0 && (CheckNormalMove(i + 1, j - 1) || CheckAttackMove(i + 1, j - 1, colorEnum)))
        {
            targets.Add(new Vector2(i + 1, j - 1));
        }
        if (i > 0 && j < 7 && (CheckNormalMove(i - 1, j + 1) || CheckAttackMove(i - 1, j + 1, colorEnum)))
        {
            targets.Add(new Vector2(i - 1, j + 1));
        }
        if (i > 0 && j > 0 && (CheckNormalMove(i - 1, j - 1) || CheckAttackMove(i - 1, j - 1, colorEnum)))
        {
            targets.Add(new Vector2(i - 1, j - 1));
        }
        if (i > 0 && (CheckNormalMove(i - 1, j) || CheckAttackMove(i - 1, j, colorEnum)))
        {
            targets.Add(new Vector2(i - 1, j));
        }
        if (i < 7 && (CheckNormalMove(i + 1, j) || CheckAttackMove(i + 1, j, colorEnum)))
        {
            targets.Add(new Vector2(i + 1, j));
        }
        if (j > 0 && (CheckNormalMove(i, j - 1) || CheckAttackMove(i, j - 1, colorEnum)))
        {
            targets.Add(new Vector2(i, j - 1));
        }
        if (j < 7 && (CheckNormalMove(i, j + 1) || CheckAttackMove(i, j + 1, colorEnum)))
        {
            targets.Add(new Vector2(i, j + 1));
        }
        return targets;
    }
    private static List<Vector2> GetPawnMoves(Vector2 currentPosition, ColorEnum colorEnum)
    {
        List<Vector2> targets = new List<Vector2>();
        if (currentPosition.y == 1 && colorEnum.Equals(ColorEnum.WHITE))
        {
            if (CheckNormalMove((int)currentPosition.x, 2))
                targets.Add(new Vector2(currentPosition.x, 2));
            if (CheckNormalMove((int)currentPosition.x, 3))
                targets.Add(new Vector2(currentPosition.x, 3));
        }
        else if (currentPosition.y + 1 <= 7 && colorEnum.Equals(ColorEnum.WHITE))
        {
            if (CheckNormalMove((int)currentPosition.x, (int)currentPosition.y + 1))
                targets.Add(new Vector2(currentPosition.x, currentPosition.y + 1));

        }
        else if (currentPosition.y == 6 && colorEnum.Equals(ColorEnum.BLACK))
        {
            if (CheckNormalMove((int)currentPosition.x, 5))
                targets.Add(new Vector2(currentPosition.x, 5));
            if (CheckNormalMove((int)currentPosition.x, 4))
                targets.Add(new Vector2(currentPosition.x, 4));
        }
        else if (currentPosition.y - 1 >= 0 && colorEnum.Equals(ColorEnum.BLACK))
        {
            if (CheckNormalMove((int)currentPosition.x, (int)currentPosition.y - 1))
                targets.Add(new Vector2(currentPosition.x, currentPosition.y - 1));
        }
        if (currentPosition.y + 1 <= 7 && colorEnum.Equals(ColorEnum.WHITE))
        {
            if (currentPosition.x + 1 <= 7 && (CheckAttackMove((int)currentPosition.x + 1, (int)currentPosition.y + 1, colorEnum) 
                || CheckEnPassantMove((int)currentPosition.x + 1, (int)currentPosition.y + 1)))
            {
                targets.Add(new Vector2(currentPosition.x + 1, currentPosition.y + 1));
            }
            if (currentPosition.x - 1 >= 0 && (CheckAttackMove((int)currentPosition.x - 1, (int)currentPosition.y + 1, colorEnum) 
                || CheckEnPassantMove((int)currentPosition.x - 1, (int)currentPosition.y + 1)))
            {
                targets.Add(new Vector2(currentPosition.x - 1, currentPosition.y + 1));
            }
        }
        else if (currentPosition.y - 1 >= 0 && colorEnum.Equals(ColorEnum.BLACK))
        {
            if (currentPosition.x + 1 <= 7 && (CheckAttackMove((int)currentPosition.x + 1, (int)currentPosition.y - 1, colorEnum)
                || CheckEnPassantMove((int)currentPosition.x + 1, (int)currentPosition.y -1)))
            {
                targets.Add(new Vector2(currentPosition.x + 1, currentPosition.y - 1));
            }
            if (currentPosition.x - 1 >= 0 && (CheckAttackMove((int)currentPosition.x - 1, (int)currentPosition.y - 1, colorEnum) 
                || CheckEnPassantMove((int)currentPosition.x - 1, (int)currentPosition.y - 1)))
            {
                targets.Add(new Vector2(currentPosition.x - 1, currentPosition.y - 1));
            }
        }
        return targets;
    }
    private static List<Vector2> GetRockMoves(Vector2 currentPosition, ColorEnum colorEnum)
    {
        List<Vector2> targets = new List<Vector2>();
        int i = (int)currentPosition.x;
        int j = (int)currentPosition.y;
        while (i < 7)
        {
            if (CheckNormalMove(i + 1, j))
            {
                targets.Add(new Vector2(++i, j));
            }
            else
            {
                if (CheckAttackMove(i + 1, j, colorEnum))
                    targets.Add(new Vector2(++i, j));
                break;
            }
        }
        i = (int)currentPosition.x;
        while (i > 0)
        {
            if (CheckNormalMove(i - 1, j))
            {
                targets.Add(new Vector2(--i, j));
            }
            else
            {
                if (CheckAttackMove(i - 1, j, colorEnum))
                    targets.Add(new Vector2(--i, j));
                break;
            }
        }
        i = (int)currentPosition.x;
        while (j > 0)
        {
            if (CheckNormalMove(i, j - 1))
            {
                targets.Add(new Vector2(i, --j));
            }
            else
            {
                if (CheckAttackMove(i, j - 1, colorEnum))
                    targets.Add(new Vector2(i, --j));
                break;
            }
        }
        j = (int)currentPosition.y;
        while (j < 7)
        {
            if (CheckNormalMove(i, j + 1))
            {
                targets.Add(new Vector2(i, ++j));
            }
            else
            {
                if (CheckAttackMove(i, j + 1, colorEnum))
                    targets.Add(new Vector2(i, ++j));
                break;
            }
        }
        return targets;
    }
    private static List<Vector2> GetQueenMoves(Vector2 currentPosition, ColorEnum colorEnum)
    {
        List<Vector2> bishopTargets = GetBishopMoves(currentPosition, colorEnum);
        List<Vector2> rockTargets = GetRockMoves(currentPosition, colorEnum);
        bishopTargets.AddRange(rockTargets);
        return bishopTargets;
    }
    private static List<Vector2> GetKnightMoves(Vector2 currentPosition, ColorEnum colorEnum)
    {
        List<Vector2> targets = new List<Vector2>();
        int i = (int)currentPosition.x;
        int j = (int)currentPosition.y;
        if (i < 6 && j < 7 && (CheckNormalMove(i + 2, j + 1) || CheckAttackMove(i + 2, j + 1, colorEnum)))
        {
            targets.Add(new Vector2(i + 2, j + 1));
        }
        if (i < 6 && j > 0 && (CheckNormalMove(i + 2, j - 1) || CheckAttackMove(i + 2, j - 1, colorEnum)))
        {
            targets.Add(new Vector2(i + 2, j - 1));
        }
        if (i > 1 && j < 7 && (CheckNormalMove(i - 2, j + 1) || CheckAttackMove(i - 2, j + 1, colorEnum)))
        {
            targets.Add(new Vector2(i - 2, j + 1));
        }
        if (i > 1 && j > 0 && (CheckNormalMove(i - 2, j - 1) || CheckAttackMove(i - 2, j - 1, colorEnum)))
        {
            targets.Add(new Vector2(i - 2, j - 1));
        }


        if (j < 6 && i < 7 && (CheckNormalMove(i + 1, j + 2) || CheckAttackMove(i + 1, j + 2, colorEnum)))
        {
            targets.Add(new Vector2(i + 1, j + 2));
        }
        if (j < 6 && i > 0 && (CheckNormalMove(i - 1, j + 2) || CheckAttackMove(i - 1, j + 2, colorEnum)))
        {
            targets.Add(new Vector2(i - 1, j + 2));
        }
        if (j > 1 && i < 7 && (CheckNormalMove(i + 1, j - 2) || CheckAttackMove(i + 1, j - 2, colorEnum)))
        {
            targets.Add(new Vector2(i + 1, j - 2));
        }
        if (j > 1 && i > 0 && (CheckNormalMove(i - 1, j - 2) || CheckAttackMove(i - 1, j - 2, colorEnum)))
        {
            targets.Add(new Vector2(i - 1, j - 2));
        }
        return targets;
    }
    private static bool CheckNormalMove(int x, int y)
    {
        if (StaticObjects.PiecesType[x, y].Equals(PieceType.NONE))
            return true;
        return false;
    }
    private static bool CheckAttackMove(int x, int y, ColorEnum currentPieceColor)
    {
        if (!StaticObjects.PiecesColor[x, y].Equals(currentPieceColor) && !StaticObjects.PiecesType[x, y].Equals(PieceType.NONE))
            return true;
        return false;
    }
    private static bool CheckEnPassantMove(int x, int y)
    {
        if (x == StaticObjects.EnPassantPosition.x && y == StaticObjects.EnPassantPosition.y)
            return true;
        return false;
    }
}
