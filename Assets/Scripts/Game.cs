﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Game : MonoBehaviour
{
    public GameObject prefabCell;
    public GameObject prefabWhitePawn;
    public GameObject prefabWhiteKing;
    public GameObject prefabWhiteQueen;
    public GameObject prefabWhiteRock;
    public GameObject prefabWhiteBishop;
    public GameObject prefabWhiteKnight;
    public GameObject prefabBlackPawn;
    public GameObject prefabBlackKing;
    public GameObject prefabBlackQueen;
    public GameObject prefabBlackRock;
    public GameObject prefabBlackBishop;
    public GameObject prefabBlackKnight;
    public static Canvas canvas;
    void Start()
    {
        canvas = GetComponent<Canvas>();
        StaticObjects.EnPassantPosition = new Vector2(-1,-1);
        StaticObjects.TakedPieces = new HashSet<Piece>();
        StaticObjects.WhitePieces = new Piece[16];
        StaticObjects.BlackPieces = new Piece[16];
        StaticObjects.Board = new GameObject[8, 8];
        StaticObjects.PiecesType = new PieceType[8, 8];
        StaticObjects.PiecesColor = new ColorEnum[8, 8];
        StaticObjects.AudioSource = GetComponent<AudioSource>();
        StaticObjects.isWhiteTurn = true;
        StaticObjects.CanMove = false;
        StaticObjects.IsCheck = false;
        StaticObjects.IsWhiteKingFirstMove = true;
        StaticObjects.IsBlackKingFirstMove = true;
        StaticObjects.WrongMove = false;
    bool white = true;
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                StaticObjects.PiecesType[j, i] = PieceType.NONE;
                StaticObjects.PiecesColor[j, i] = ColorEnum.NONE;
                GameObject cell = Instantiate(prefabCell);
                Image image = cell.GetComponent<Image>();
                if (white)
                {
                    image.color = new Color(94/255f,60/255f,60/255f);
                    white = false;
                }
                else
                {
                    image.color = new Color(166/255f,166/255f,166/255f);
                    white = true;
                }
                cell.transform.SetParent(gameObject.transform);
                RectTransform rectTransform = cell.GetComponent<RectTransform>();
                rectTransform.anchoredPosition = new Vector3(-320 + (j * 91), -386 + (i * 91), 1);
                rectTransform.localScale = new Vector3(1.5f, 1.5f, 1);
                StaticObjects.Board[i, j] = cell;
                cell.GetComponent<DropToCell>().position.Set(j, i);
            }
            if (white)
                white = false;
            else
                white = true;
        }

        for (int i = 0; i < 8; i++)
        {
            SetPieces(prefabWhitePawn, i, 1, ColorEnum.WHITE, 8 + i, PieceType.PAWN);
            SetPieces(prefabBlackPawn, i, 6, ColorEnum.BLACK, 8 + i, PieceType.PAWN);
        }
        for (int i = 0; i <= 7; i += 7)
        {
            SetPieces(prefabWhiteRock, i, 0, ColorEnum.WHITE, i, PieceType.ROCK);
            SetPieces(prefabBlackRock, i, 7, ColorEnum.BLACK, i, PieceType.ROCK);
        }
        for (int i = 1; i <= 6; i += 5)
        {
            SetPieces(prefabWhiteKnight, i, 0, ColorEnum.WHITE, i, PieceType.KNIGHT);
            SetPieces(prefabBlackKnight, i, 7, ColorEnum.BLACK, i, PieceType.KNIGHT);
        }
        for (int i = 2; i <= 5; i += 3)
        {
            SetPieces(prefabWhiteBishop, i, 0, ColorEnum.WHITE, i, PieceType.BISHOP);
            SetPieces(prefabBlackBishop, i, 7, ColorEnum.BLACK, i, PieceType.BISHOP);
        }
        SetPieces(prefabWhiteKing, 4, 0, ColorEnum.WHITE, 4, PieceType.KING);
        SetPieces(prefabWhiteQueen, 3, 0, ColorEnum.WHITE, 3, PieceType.QUEEN);
        SetPieces(prefabBlackKing, 4, 7, ColorEnum.BLACK, 4, PieceType.KING);
        SetPieces(prefabBlackQueen, 3, 7, ColorEnum.BLACK, 3, PieceType.QUEEN);

    }

    private void SetPieces(GameObject prefab, int i, int j, ColorEnum colorEnum, int index, PieceType pieceType)
    {
        GameObject pieceObject = Instantiate(prefab);
        Piece piece = pieceObject.GetComponent<Piece>();
        if(pieceType.Equals(PieceType.PAWN))
        piece.tag = pieceType + "" + i;
        if (colorEnum.Equals(ColorEnum.WHITE))
        {
            StaticObjects.WhitePieces[index] = piece;
        }
        else
        {
            StaticObjects.BlackPieces[index] = piece;
        }
        piece.Color = colorEnum;
        piece.PieceType = pieceType;
        piece.Position = StaticObjects.Board[j, i].GetComponent<DropToCell>().position;
        StaticObjects.PiecesType[i, j] = pieceType;
        StaticObjects.PiecesColor[i, j] = colorEnum;
        RectTransform rectTransform = pieceObject.GetComponent<RectTransform>();
        pieceObject.transform.SetParent(gameObject.transform);
        rectTransform.localScale = new Vector3(80, 80, 0);
        rectTransform.localPosition = new Vector3(StaticObjects.Board[j, i].transform.localPosition.x, StaticObjects.Board[j, i].transform.localPosition.y, 0);
    }
}
