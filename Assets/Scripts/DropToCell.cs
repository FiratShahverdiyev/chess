﻿using UnityEngine;
using UnityEngine.EventSystems;

public class DropToCell : MonoBehaviour, IDropHandler
{
    public Vector2 position;
    private bool flag = false;
    public void OnDrop(PointerEventData eventData)
    {
        if (!StaticObjects.CanMove)
            return;
        if (eventData.pointerDrag != null)
        {
            Piece piece = eventData.pointerDrag.GetComponent<Piece>();
            EnPassant(piece);
            TakeAfterEnPassant(piece);
            if (piece.PieceType.Equals(PieceType.KING))
            {
                Rock(piece, position);
                SetRockFalse(piece);
            }
            Vector2 oldPosition = piece.Position;
            Vector2 oldAnchoredPosition = StaticObjects.Board[(int)oldPosition.y,(int)oldPosition.x].GetComponent<RectTransform>().anchoredPosition;
            Helper.PieceToTargetPosition(piece, GetComponent<RectTransform>().anchoredPosition, position);
            Helper.EnemyPiecesTargetPositionsContainsCheck(piece.Color);
            if (StaticObjects.WrongMove)
            {
                Helper.PieceToTargetPosition(piece, oldAnchoredPosition , oldPosition);
                StaticObjects.WrongMove = false;
                Helper.RollBackMove(piece.Color);
                return;
            }
            if(!flag)
            StaticObjects.EnPassantPosition = new Vector2(-1, -1);

        }
    }
    private void EnPassant(Piece piece)
    {
        if (piece.PieceType.Equals(PieceType.PAWN))
        {
            if (piece.Color.Equals(ColorEnum.BLACK) && piece.Position.y == 6 && position.y == 4)
            {
                StaticObjects.EnPassantPosition = new Vector2(position.x, 5);
                flag = true;
            }
            else if (piece.Color.Equals(ColorEnum.WHITE) && piece.Position.y == 1 && position.y == 3)
            {
                StaticObjects.EnPassantPosition = new Vector2(position.x, 2);
                flag = true;
            }
            else
                flag = false;
        }
    }
    private void TakeAfterEnPassant(Piece piece)
    {
        if (piece.PieceType.Equals(PieceType.PAWN))
        {
            if (piece.Color.Equals(ColorEnum.BLACK) && position.Equals(StaticObjects.EnPassantPosition))
            {
                GameObject[] pawns = GameObject.FindGameObjectsWithTag("PAWN"+ position.x);
                foreach (GameObject pawn in pawns)
                {
                    if (pawn.GetComponent<Piece>().Color.Equals(ColorEnum.WHITE))
                    {
                        Destroy(pawn);
                        StaticObjects.TakedPieces.Add(pawn.GetComponent<Piece>());
                    }
                }
            }
            else if (piece.Color.Equals(ColorEnum.WHITE) && position.Equals(StaticObjects.EnPassantPosition))
            {
                GameObject[] pawns = GameObject.FindGameObjectsWithTag("PAWN" + position.x);
                foreach (GameObject pawn in pawns)
                {
                    if (pawn.GetComponent<Piece>().Color.Equals(ColorEnum.BLACK))
                    {
                        Destroy(pawn);
                        StaticObjects.TakedPieces.Add(pawn.GetComponent<Piece>());
                    }
                }
            }
        }
    }
    private void Rock(Piece piece, Vector2 targetPosition)
    {
        if (IsShortRock(piece, targetPosition))
        {
            ShortRock(piece.Color);
        }
        else if (IsLongRock(piece, targetPosition))
        {
            LongRock(piece.Color);
        }
    }
    private void SetRockFalse(Piece piece)
    {
        if (piece.Color.Equals(ColorEnum.WHITE))
        {
            StaticObjects.IsWhiteKingFirstMove = false;
        }
        else if (piece.Color.Equals(ColorEnum.BLACK))
        {
            StaticObjects.IsBlackKingFirstMove = false;
        }
    }
    private bool IsShortRock(Piece piece, Vector2 target)
    {
        if (target.x - piece.Position.x == 2)
        {
            return true;
        }
        return false;
    }
    private bool IsLongRock(Piece piece, Vector2 target)
    {
        if (piece.Position.x - target.x == 2)
        {
            return true;
        }
        return false;
    }
    private void LongRock(ColorEnum color)
    {
        if (color.Equals(ColorEnum.BLACK))
        {
            Piece rightBlackRock = StaticObjects.BlackPieces[0];
            DropToCell rockTargetCell = StaticObjects.Board[7, 3].GetComponent<DropToCell>();
            Helper.PieceToTargetPosition(rightBlackRock, rockTargetCell.GetComponent<RectTransform>().anchoredPosition, rockTargetCell.position);
        }
        else if (color.Equals(ColorEnum.WHITE))
        {
            Piece rightWhiteRock = StaticObjects.WhitePieces[0];
            DropToCell rockTargetCell = StaticObjects.Board[0, 3].GetComponent<DropToCell>();
            Helper.PieceToTargetPosition(rightWhiteRock, rockTargetCell.GetComponent<RectTransform>().anchoredPosition, rockTargetCell.position);
        }
    }
    private void ShortRock(ColorEnum color)
    {
        if (color.Equals(ColorEnum.BLACK))
        {
            Piece rightBlackRock = StaticObjects.BlackPieces[7];
            DropToCell rockTargetCell = StaticObjects.Board[7, 5].GetComponent<DropToCell>();
            Helper.PieceToTargetPosition(rightBlackRock, rockTargetCell.GetComponent<RectTransform>().anchoredPosition, rockTargetCell.position);
        }
        else if (color.Equals(ColorEnum.WHITE))
        {
            Piece rightWhiteRock = StaticObjects.WhitePieces[7];
            DropToCell rockTargetCell = StaticObjects.Board[0, 5].GetComponent<DropToCell>();
            Helper.PieceToTargetPosition(rightWhiteRock, rockTargetCell.GetComponent<RectTransform>().anchoredPosition, rockTargetCell.position);
        }
    }
}
