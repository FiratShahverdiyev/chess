﻿using System.Collections.Generic;
using UnityEngine;

public class StaticObjects : MonoBehaviour
{
    public static AudioSource AudioSource { get; set; }
    public static GameObject[,] Board { get; set; }
    public static Vector2 EnPassantPosition { get; set; }
    public static PieceType[,] PiecesType { get; set; }
    public static ColorEnum[,] PiecesColor { get; set; }
    public static Piece[] WhitePieces { get; set; }
    public static Piece[] BlackPieces { get; set; }
    public static HashSet<Piece> TakedPieces { get; set; } 
    public static bool isWhiteTurn { get; set; }
    public static bool CanMove { get; set; }
    public static bool IsCheck { get; set; }
    public static bool IsWhiteKingFirstMove { get; set; }
    public static bool IsBlackKingFirstMove { get; set; }
    public static bool WrongMove { get; set; }
}
