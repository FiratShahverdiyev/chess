﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Piece : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler
{
    public Vector2 Position { get; set; }
    public List<Vector2> Targets { get; set; }
    public PieceType PieceType { get; set; }
    public ColorEnum Color { get; set; }
    private RectTransform rectTransform;
    private CanvasGroup canvasGroup;
    private Queue<Color> colors;
    private Vector2 oldPosition;
    private static Vector2 checkPosition;
    private static Vector2 attackerPiecePosition;
    private void Awake()
    {
        colors = new Queue<Color>();
        rectTransform = GetComponent<RectTransform>();
        canvasGroup = GetComponent<CanvasGroup>();
    }

    public void OnDrag(PointerEventData eventData)
    {
        rectTransform.anchoredPosition += eventData.delta / Game.canvas.scaleFactor;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Helper.MarkDestroyedPiece(null);
        StaticObjects.CanMove = false;
        canvasGroup.blocksRaycasts = false;
        oldPosition = rectTransform.anchoredPosition;
        Targets = PossibleMovesCalculator.GetPossibleMoves(PieceType, Position, Color);
        if (StaticObjects.IsCheck)
        {
            FilterTargetForCheckCase();
        }
        if (PieceType.Equals(PieceType.KING))
        {
            if (CheckShortRock() && !StaticObjects.IsCheck)
                ShortRockAddToTarget();
            if (CheckLongRock() && !StaticObjects.IsCheck)
                LongRockAddToTarget();
        }
        TargetCellColorSwitchToBlue();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        canvasGroup.blocksRaycasts = true;
        AllCellSwitchToOldColor();
        if (!CheckChangeTurn())
        {
            WrongMove();
            return;
        }
        bool isTarget = CheckTargetPositions(eventData);
        bool isAttackTarget = CheckAttackPositions(eventData);
        if (!isTarget && !isAttackTarget)
        {
            WrongMove();
            return;
        }
        if (isAttackTarget)
        {
            Vector2 oldPosition = Position;
            Vector2 oldAnchoredPosition = StaticObjects.Board[(int)oldPosition.y, (int)oldPosition.x].GetComponent<RectTransform>().anchoredPosition;
            Helper.PieceToTargetPosition(this, eventData.pointerCurrentRaycast.gameObject.GetComponent<RectTransform>().anchoredPosition,
                eventData.pointerCurrentRaycast.gameObject.GetComponent<Piece>().Position);
            Helper.MarkDestroyedPiece(eventData.pointerCurrentRaycast.gameObject.GetComponent<Piece>());
            Helper.EnemyPiecesTargetPositionsContainsCheck(Color);
            if (StaticObjects.WrongMove)
            {
                Position = oldPosition;
                GetComponent<RectTransform>().anchoredPosition = oldAnchoredPosition;
                Piece targetPiece = eventData.pointerCurrentRaycast.gameObject.GetComponent<Piece>();
                StaticObjects.PiecesColor[(int)targetPiece.Position.x, (int)targetPiece.Position.y] = targetPiece.Color;
                StaticObjects.PiecesType[(int)targetPiece.Position.x, (int)targetPiece.Position.y] = targetPiece.PieceType;
                StaticObjects.PiecesColor[(int)oldPosition.x, (int)oldPosition.y] = Color;
                StaticObjects.PiecesType[(int)oldPosition.x, (int)oldPosition.y] = PieceType;
                StaticObjects.WrongMove = false;
                return;
            }
            DestroyTargetPiece(eventData.pointerCurrentRaycast.gameObject);
            ChangeTurn();
            //After playing move calculate new targets and control isCheck or not
        StaticObjects.EnPassantPosition = new Vector2(-1, -1);

            List<Vector2> targets = PossibleMovesCalculator.GetPossibleMoves(PieceType, Position, Color);
            IsCheck(targets, Position);
        }
        else if (isTarget)
        {
            ChangeTurn();
            List<Vector2> targets = PossibleMovesCalculator.GetPossibleMoves(PieceType,
                eventData.pointerCurrentRaycast.gameObject.GetComponent<DropToCell>().position, Color);
            /* EnemyPiecesTargetPositionsContainsCheck();*/
            IsCheck(targets, eventData.pointerCurrentRaycast.gameObject.GetComponent<DropToCell>().position);
        }
    }
    private void IsCheck(List<Vector2> targets, Vector2 attackerPiecePosition)
    {
        targets.ForEach(target =>
        {
            if (StaticObjects.PiecesType[(int)target.x, (int)target.y].Equals(PieceType.KING))
            {
                if (Color != StaticObjects.PiecesColor[(int)target.x, (int)target.y])
                {
                    checkPosition = target;
                    Piece.attackerPiecePosition = attackerPiecePosition;
                    StaticObjects.IsCheck = true;
                    PlayCheckSound();
                    return;
                }
            }
        });
    }
    private void FilterTargetForCheckCase()
    {
        List<Vector2> defenseCheckFromAttack = DefenseFromAttackToCheckPositions();
        List<Vector2> copyTargets = new List<Vector2>(Targets);
        Vector2 attackerPiecePosition = defenseCheckFromAttack[0];
        if (!PieceType.Equals(PieceType.KING))
            Targets.Clear();
        defenseCheckFromAttack.ForEach(defensedCell =>
        {
            if (copyTargets.Contains(defensedCell) && !PieceType.Equals(PieceType.KING))
            {
                Targets.Add(defensedCell);
            }
            else if (copyTargets.Contains(defensedCell) && PieceType.Equals(PieceType.KING))
            {
                if (!defensedCell.Equals(attackerPiecePosition))
                    Targets.Remove(defensedCell);
            }
        });
    }
    private List<Vector2> DefenseFromAttackToCheckPositions()
    {
        List<Vector2> defensePosition = new List<Vector2>();
        float x = attackerPiecePosition.x;
        float y = attackerPiecePosition.y;
        defensePosition.Add(new Vector2(x, y));
        while (x < checkPosition.x && y < checkPosition.y)
        {
            defensePosition.Add(new Vector2(++x, ++y));
        }
        while (x > checkPosition.x && y > checkPosition.y)
        {
            defensePosition.Add(new Vector2(--x, --y));
        }
        while (x < checkPosition.x && y > checkPosition.y)
        {
            defensePosition.Add(new Vector2(++x, --y));
        }
        while (x > checkPosition.x && y < checkPosition.y)
        {
            defensePosition.Add(new Vector2(--x, ++y));
        }
        while (x > checkPosition.x && y == checkPosition.y)
        {
            defensePosition.Add(new Vector2(--x, y));
        }
        while (x < checkPosition.x && y == checkPosition.y)
        {
            defensePosition.Add(new Vector2(++x, y));
        }
        while (x == checkPosition.x && y < checkPosition.y)
        {
            defensePosition.Add(new Vector2(x, ++y));
        }
        while (x == checkPosition.x && y > checkPosition.y)
        {
            defensePosition.Add(new Vector2(x, --y));
        }
        return defensePosition;
    }
    private void AllCellSwitchToOldColor()
    {
        Targets.ForEach(target =>
        {
            if (colors.Count >= 1)
                StaticObjects.Board[(int)target.y, (int)target.x].GetComponent<Image>().color = colors.Dequeue();
        });
    }
    private bool CheckTargetPositions(PointerEventData eventData)
    {
        bool flag = false;
        Targets.ForEach(target =>
        {
            try
            {
                if (eventData.pointerCurrentRaycast.gameObject.GetComponent<DropToCell>() != null)
                {
                    if (target.Equals(eventData.pointerCurrentRaycast.gameObject.GetComponent<DropToCell>().position))
                    {
                        flag = true;
                    }
                }
            }
            catch
            {
                Debug.Log("Error");
                StaticObjects.CanMove = false;
                rectTransform.anchoredPosition = oldPosition;
            }
        });
        return flag;
    }
    private void TargetCellColorSwitchToBlue()
    {
        Targets.ForEach(target =>
        {
            colors.Enqueue(StaticObjects.Board[(int)target.y, (int)target.x].GetComponent<Image>().color);
            if (!StaticObjects.PiecesType[(int)target.x, (int)target.y].Equals(PieceType.KING))
                StaticObjects.Board[(int)target.y, (int)target.x].GetComponent<Image>().color = UnityEngine.Color.blue;
            else
                StaticObjects.Board[(int)target.y, (int)target.x].GetComponent<Image>().color = UnityEngine.Color.grey;
        });
    }
    private bool CheckAttackPositions(PointerEventData eventData)
    {
        bool flag = false;
        if (eventData.pointerCurrentRaycast.gameObject != null)
            if (eventData.pointerCurrentRaycast.gameObject.GetComponent<Piece>() != null &&
                Targets.Contains(eventData.pointerCurrentRaycast.gameObject.GetComponent<Piece>().Position))
            {
                if (eventData.pointerCurrentRaycast.gameObject.GetComponent<Piece>().Color != this.Color
                    && !eventData.pointerCurrentRaycast.gameObject.GetComponent<Piece>().PieceType.Equals(PieceType.KING))
                {
                    flag = true;
                }
            }
        return flag;
    }
    private bool CheckChangeTurn()
    {
        if (StaticObjects.isWhiteTurn)
        {
            if (Color.Equals(ColorEnum.WHITE))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            if (Color.Equals(ColorEnum.BLACK))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    private void ChangeTurn()
    {
        if (Color.Equals(ColorEnum.BLACK))
        {
            StaticObjects.isWhiteTurn = true;
            StaticObjects.CanMove = true;
        }
        else
        {
            StaticObjects.isWhiteTurn = false;
            StaticObjects.CanMove = true;
        }
        StaticObjects.IsCheck = false;
        PlayPieceDropSound();
    }
    private void DestroyTargetPiece(GameObject piece)
    {
        if (Color.Equals(ColorEnum.WHITE))
        {
            StaticObjects.TakedPieces.Add(piece.GetComponent<Piece>());
            Debug.Log("Removed from BlackPiece : " + piece.GetComponent<Piece>().PieceType);
        }
        else if (Color.Equals(ColorEnum.BLACK))
        {
            StaticObjects.TakedPieces.Add(piece.GetComponent<Piece>());
            Debug.Log("Removed from WhitePiece : " + piece.GetComponent<Piece>().PieceType);
        }
        Destroy(piece);
    }
    private void PlayPieceDropSound()
    {
        StaticObjects.AudioSource.PlayOneShot(Resources.Load<AudioClip>("Sounds/pieceDrop"));
    }
    private void PlayCheckSound()
    {
        StaticObjects.AudioSource.PlayOneShot(Resources.Load<AudioClip>("Sounds/check"));
    }
    private void WrongMove()
    {
        StaticObjects.CanMove = false;
        rectTransform.anchoredPosition = oldPosition;
        return;
    }
    private bool CheckShortRock()
    {
        if (!StaticObjects.IsCheck && StaticObjects.IsWhiteKingFirstMove && Color.Equals(ColorEnum.WHITE) && StaticObjects.PiecesType[7, 0].Equals(PieceType.ROCK))
        {
            if (StaticObjects.PiecesType[6, 0].Equals(PieceType.NONE) && StaticObjects.PiecesType[5, 0].Equals(PieceType.NONE))
            {
                return true;
            }
        }
        else if (!StaticObjects.IsCheck && StaticObjects.IsBlackKingFirstMove && Color.Equals(ColorEnum.BLACK) && StaticObjects.PiecesType[7, 7].Equals(PieceType.ROCK))
        {
            if (StaticObjects.PiecesType[6, 7].Equals(PieceType.NONE) && StaticObjects.PiecesType[5, 7].Equals(PieceType.NONE))
            {
                return true;
            }
        }
        return false;
    }
    private void ShortRockAddToTarget()
    {
        Targets.Add(new Vector2(Position.x + 2, Position.y));
    }
    private bool CheckLongRock()
    {
        if (StaticObjects.IsWhiteKingFirstMove && Color.Equals(ColorEnum.WHITE) && StaticObjects.PiecesType[0, 0].Equals(PieceType.ROCK))
        {
            if (StaticObjects.PiecesType[1, 0].Equals(PieceType.NONE) && StaticObjects.PiecesType[2, 0].Equals(PieceType.NONE)
                && StaticObjects.PiecesType[3, 0].Equals(PieceType.NONE))
            {
                return true;
            }
        }
        else if (StaticObjects.IsBlackKingFirstMove && Color.Equals(ColorEnum.BLACK) && StaticObjects.PiecesType[0, 7].Equals(PieceType.ROCK))
        {
            if (StaticObjects.PiecesType[1, 7].Equals(PieceType.NONE) && StaticObjects.PiecesType[2, 7].Equals(PieceType.NONE)
                && StaticObjects.PiecesType[3, 7].Equals(PieceType.NONE))
            {
                return true;
            }
        }
        return false;
    }
    private void LongRockAddToTarget()
    {
        Targets.Add(new Vector2(Position.x - 2, Position.y));
    }
}
