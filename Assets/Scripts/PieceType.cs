﻿public enum PieceType
{
   KNIGHT,
   ROCK,
   PAWN,
   QUEEN,
   KING,
   BISHOP,
   NONE
}
