﻿using UnityEngine;

public class Helper
{
    private static Piece destroyedPiece;
    public static void PieceToTargetPosition(Piece piece, Vector2 anchoredPosition, Vector2 targetPosition)
    {
        piece.GetComponent<RectTransform>().anchoredPosition = anchoredPosition;
        StaticObjects.PiecesColor[(int)piece.Position.x, (int)piece.Position.y] = ColorEnum.NONE;
        StaticObjects.PiecesType[(int)piece.Position.x, (int)piece.Position.y] = PieceType.NONE;
        piece.Position = targetPosition;
        StaticObjects.PiecesColor[(int)piece.Position.x, (int)piece.Position.y] = piece.Color;
        StaticObjects.PiecesType[(int)piece.Position.x, (int)piece.Position.y] = piece.PieceType;
    }
    public static void EnemyPiecesTargetPositionsContainsCheck(ColorEnum color)
    {
        if (color.Equals(ColorEnum.BLACK))
        {
            for (int i = 0; i < 16; i++)
            {
                if (StaticObjects.WhitePieces[i].Equals(destroyedPiece) || StaticObjects.TakedPieces.Contains(StaticObjects.WhitePieces[i]))
                    continue;
                StaticObjects.WhitePieces[i].Targets = PossibleMovesCalculator.GetPossibleMoves(StaticObjects.WhitePieces[i].PieceType,
                   StaticObjects.WhitePieces[i].Position, StaticObjects.WhitePieces[i].Color);

                if (StaticObjects.WhitePieces[i].Targets.Contains(StaticObjects.BlackPieces[4].Position))
                {
                    StaticObjects.WrongMove = true;
                    Debug.Log("Check under attack!");
                    return;
                }
            }
        }
        else if (color.Equals(ColorEnum.WHITE))
        {
            for (int i = 0; i < 16; i++)
            {
                if (StaticObjects.BlackPieces[i].Equals(destroyedPiece) || StaticObjects.TakedPieces.Contains(StaticObjects.BlackPieces[i]))
                    continue;
                StaticObjects.BlackPieces[i].Targets = PossibleMovesCalculator.GetPossibleMoves(StaticObjects.BlackPieces[i].PieceType,
                    StaticObjects.BlackPieces[i].Position, StaticObjects.BlackPieces[i].Color);

                if (StaticObjects.BlackPieces[i].Targets.Contains(StaticObjects.WhitePieces[4].Position))
                {
                    StaticObjects.WrongMove = true;
                    Debug.Log("Check under attack!");
                    return;
                }
            }
        }
    }
    public static void MarkDestroyedPiece(Piece piece)
    {
        destroyedPiece = piece;
    }
    public static void RollBackMove(ColorEnum color)
    {
        if (color.Equals(ColorEnum.BLACK))
        {
            StaticObjects.isWhiteTurn = false;
            StaticObjects.CanMove = false;
        }
        else
        {
            StaticObjects.isWhiteTurn = true;
            StaticObjects.CanMove = false;
        }
    }
}
